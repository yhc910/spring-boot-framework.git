spring-boot-framework

本框架是基于springboot V3.0.2来搭建的，已经引入了nacos、mybatisplus、feign、mysql等组件，也包含各种统一处理，可以直接入手编写业务代码。
1、spring-boot-framework-api，为提供外部调用的接口，一般外部引用此api，即可像本地调用一样方便，无需再次编写调用代码。（注：demo里的login相关接口为内部接口，不应放在api里，仅为示例效果）
2、spring-boot-framework-common，一些基本的工具类，微服务架构时，应单独出此模块为一个公共模块，公共枚举、参数定义、工具类应放于该模块中
3、spring-boot-framework-server，具体的业务实现，因引入了mybatisplus，所以多了一层manager，主要是针对单表的mybatisplus方法

此框架为本人亲手搭建，如有雷同，纯属巧合。该框架中，仅集成了登录的功能。相信框架中还有很多可以优化的地方，还希望大家可以指出。
springboot3.x内置了声明式HTTP客户端@HttpExchange，但是搭建和使用过程中明显没有openfeign好用（可能是我还没有足够的了解，这里仅供参考）