package com.yhc.server.service;

public interface ITestService {

    void set(long id);

    String get(long id);

}
