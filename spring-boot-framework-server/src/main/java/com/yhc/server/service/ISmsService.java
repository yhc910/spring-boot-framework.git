package com.yhc.server.service;

public interface ISmsService {

    void send(String mobile, String code);

    void codeVerify(String mobile, String validCode);
}
