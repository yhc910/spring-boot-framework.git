package com.yhc.server.service;

import com.yhc.api.request.LoginRequest;
import com.yhc.api.response.LoginResponse;

public interface LoginService {

    void sendMsg(String msg);

    void getCaptcha(String randomStr);

    LoginResponse login(LoginRequest request);

}
