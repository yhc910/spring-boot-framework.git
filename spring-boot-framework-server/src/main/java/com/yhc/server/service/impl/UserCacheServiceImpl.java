package com.yhc.server.service.impl;

import com.yhc.common.dto.UserCacheInfo;
import com.yhc.server.exception.ErrorCodeEnum;
import com.yhc.server.exception.SBFServiceException;
import com.yhc.server.service.UserCacheService;
import org.springframework.stereotype.Service;

@Service
public class UserCacheServiceImpl implements UserCacheService {

    private final ThreadLocal<UserCacheInfo> userTl = new ThreadLocal<>();

    @Override
    public void setUser(UserCacheInfo user) {
        userTl.set(user);
    }

    @Override
    public UserCacheInfo getUser() {
        return userTl.get();
    }

    @Override
    public long getUserId() {
        return userTl.get() == null ? null : userTl.get().getUserId();
    }

    @Override
    public long getUserIdWithException() {
        UserCacheInfo user = userTl.get();
        if (user == null) {
            throw new SBFServiceException(ErrorCodeEnum.EXCEPTION_USER_NO_LOGIN);
        }
        return user.getUserId();
    }
}
