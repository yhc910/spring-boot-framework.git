package com.yhc.server.service.impl;

import com.yhc.common.util.UuidUtil;
import com.yhc.server.plugin.RedisService;
import com.yhc.server.service.ITestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TestServiceImpl implements ITestService {

    @Autowired
    RedisService redisService;

    @Override
    public void set(long id) {
        redisService.set(String.valueOf(id), UuidUtil.getUuid());
    }

    @Override
    public String get(long id) {
        return redisService.get(String.valueOf(id));
    }

}
