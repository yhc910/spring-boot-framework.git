package com.yhc.server.service;


import com.yhc.common.dto.UserCacheInfo;

public interface UserCacheService {

    void setUser(UserCacheInfo user);

    /**
     * 获取用户缓存信息
     * @return
     */
    UserCacheInfo getUser();

    /**
     * 获取当前登录的用户ID
     * @return
     */
    long getUserId();

    /**
     * 获取当前登录的用户ID（不存在则抛错）
     * @return
     */
    long getUserIdWithException();

}
