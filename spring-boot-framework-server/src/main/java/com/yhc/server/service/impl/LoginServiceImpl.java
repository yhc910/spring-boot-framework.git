package com.yhc.server.service.impl;

import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.core.util.RandomUtil;
import com.yhc.api.request.LoginRequest;
import com.yhc.api.response.LoginResponse;
import com.yhc.common.util.ServletUtil;
import com.yhc.common.util.UuidUtil;
import com.yhc.server.consts.RedisPrefixKey;
import com.yhc.server.consts.SystemValue;
import com.yhc.server.entity.UserEntity;
import com.yhc.server.exception.SBFServiceException;
import com.yhc.server.manager.UserManager;
import com.yhc.server.plugin.RedisService;
import com.yhc.server.plugin.SendMessageService;
import com.yhc.server.service.ISmsService;
import com.yhc.server.service.LoginService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    RedisService redisService;
    @Autowired
    SendMessageService sendMsgService;
    @Autowired
    UserManager userManager;
    @Autowired
    ISmsService smsService;

    private static final Long TOKEN_EXPIRE = (long) 24 * 60 * 60;


    @Override
    public LoginResponse login(LoginRequest request) {
        // 校验图形验证码
        String cacheCaptchaKey = String.format(RedisPrefixKey.LOGIN_CAPTCHA, request.getRandomStr());
        String cacheCaptcha = redisService.get(cacheCaptchaKey);
        if (!StringUtils.equals(cacheCaptcha, request.getCaptcha())) {
            throw new SBFServiceException("验证码错误。");
        }
        // 清理验证码
        redisService.delete(cacheCaptchaKey);

        smsService.codeVerify(request.getMobile(),request.getSmsCode());

        // 校验用户（DEMO这里暂且仅支持手动注册）
        UserEntity user = userManager.getOneByMobile(request.getMobile());
        if (user == null) {
            throw new SBFServiceException("使用者账号未认证，请联系客服进行账号认证");
        }

        // 生成token
        String token = generateToken(user.getName());
        redisService.set(String.format(RedisPrefixKey.LOGIN_USER_TOKEN, user.getId()), token, TOKEN_EXPIRE);
        redisService.set(String.format(RedisPrefixKey.LOGIN_USER_TOKEN, token), user.getId(), TOKEN_EXPIRE);

        return LoginResponse.builder().name(user.getName()).tk(token).build();
    }

    @Override
    public void sendMsg(String mobile) {
        String smsCode = RandomUtil.randomNumbers(6);
        smsService.send(mobile,smsCode);
    }

    public void getCaptcha(String randomStr) {
        CircleCaptcha captcha = new CircleCaptcha(200, 100, 4, 20);
        String result = captcha.getCode();
        // 保存验证码信息
        redisService.set(String.format(RedisPrefixKey.LOGIN_CAPTCHA, randomStr), result, 60);
        try {
            HttpServletResponse response = ServletUtil.getResponse();
            captcha.write(response.getOutputStream());
        } catch (IOException ie) {
            throw new SBFServiceException("获取验证码失败");
        }
    }

    private String generateToken(String nickName) {
        return SystemValue.ABBREVIATION_APPLICATION_NAME + ":" + nickName + ":" + UuidUtil.getUuid();
    }

}
