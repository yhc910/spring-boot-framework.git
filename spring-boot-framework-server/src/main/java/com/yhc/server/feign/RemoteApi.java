package com.yhc.server.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "remote", url = "http://www.baidu.com")
public interface RemoteApi {

//    @RequestMapping
//    default String remote() {
//        return "remote";
//    }

    @RequestMapping
    String remote();

}
