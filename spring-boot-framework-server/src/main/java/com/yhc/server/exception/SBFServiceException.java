package com.yhc.server.exception;


import com.yhc.common.exception.ResponseEnum;
import com.yhc.common.exception.ServiceException;
import lombok.Getter;

@Getter
public class SBFServiceException extends ServiceException {

    private final int errorCode;
    private final String errorMessage;

    /**
     * 建议使用此方法，除非需要特定错误码判断场景
     *
     * @param errorMessage
     */
    public SBFServiceException(String errorMessage) {
        super(errorMessage);
        this.errorCode = ResponseEnum.EXCEPTION_CUSTOMIZE.getCode();
        this.errorMessage = errorMessage;
    }

    public SBFServiceException(String errorMessage, Object... args) {
        super(errorMessage);
        errorMessage = String.format(errorMessage, args);
        this.errorCode = ResponseEnum.EXCEPTION_CUSTOMIZE.getCode();
        this.errorMessage = errorMessage;
    }

    public SBFServiceException(ErrorCodeEnum error) {
        super(error.getCode() + "-" + error.getMessage());
        this.errorCode = error.getCode();
        this.errorMessage = error.getMessage();
    }

}
