package com.yhc.server.exception;

import lombok.Getter;

@Getter
public enum ErrorCodeEnum {

    EXCEPTION_TEST(400101, "自定义异常"),
    EXCEPTION_REQUIRED_PARAMS_NO_REQUEST_ID(400102, "no requestId"),
    EXCEPTION_REQUIRED_PARAMS_NO_TX_TIME(400103, "no txTime"),
    EXCEPTION_USER_NO_LOGIN(400104, "用户未登录"),
    EXCEPTION_USER_SQUEEZE_LOGIN(400105, "您已在其它地方登录，若非本人操作，请及时联系客服处理！"),

    ;
    
    int code;
    String message;

    ErrorCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

}
