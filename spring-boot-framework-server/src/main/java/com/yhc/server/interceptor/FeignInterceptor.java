package com.yhc.server.interceptor;

import com.yhc.common.util.ServletUtil;
import com.yhc.server.consts.RequestHeader;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class FeignInterceptor implements RequestInterceptor {


    public void apply(RequestTemplate template) {
        log.info("# FeignHeaderRequestInterceptor");

        HttpServletRequest request = ServletUtil.getRequest();

        template.header(RequestHeader.REQUEST_ID, request.getHeader(RequestHeader.REQUEST_ID));
//        template.header(RequestHeader.TX_TIME, request.getHeader(RequestHeader.TX_TIME));

        // 解决seata的xid未传递
//        String xid = RootContext.getXID();
//        template.header(RootContext.KEY_XID, xid);
    }

}
