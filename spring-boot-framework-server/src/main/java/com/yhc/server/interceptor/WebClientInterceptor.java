package com.yhc.server.interceptor;

import com.yhc.common.util.ServletUtil;
import com.yhc.server.consts.RequestHeader;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class WebClientInterceptor implements ExchangeFilterFunction {

    @Override
    public Mono<ClientResponse> filter(ClientRequest request, ExchangeFunction next) {
        log.info("# WebClientInterceptor");

        HttpServletRequest httpRequest = ServletUtil.getRequest();

        request.headers().set(RequestHeader.REQUEST_ID, httpRequest.getHeader(RequestHeader.REQUEST_ID));
//        template.header(RequestHeader.TX_TIME, request.getHeader(RequestHeader.TX_TIME));

        // 解决seata的xid未传递
//        String xid = RootContext.getXID();
//        template.header(RootContext.KEY_XID, xid);
        return next.exchange(request);
    }
}
