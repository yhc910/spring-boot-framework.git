package com.yhc.server.plugin;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SendMessageService {

    /**
     * 请自行接入相应短信渠道
     *
     * @param mobile
     * @param code
     */
    public void sendSms(String mobile, String code) {
        // TODO 调用短信服务发送短信
        log.info("# 发送短信成功: [mobile: {}, code: {}]", mobile, code);
    }

}
