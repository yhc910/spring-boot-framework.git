package com.yhc.server.plugin;

import com.fasterxml.jackson.core.type.TypeReference;
import com.yhc.common.util.JacksonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis工具类，此处所有有效期均以秒为单位来计算
 */
@Slf4j
@Configuration
public class RedisService {


    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * SET操作
     *
     * @param key
     * @param value
     * @return 是否成功
     */
    public boolean set(String key, String value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            log.warn("# redis set操作异常", e);
            return false;
        }
    }

    public boolean set(String key, String value, long expire) {
        try {
            redisTemplate.opsForValue().set(key, value, expire, TimeUnit.SECONDS);
            return true;
        } catch (Exception e) {
            log.warn("# redis set操作异常", e);
            return false;
        }
    }

    /**
     * GET操作
     *
     * @param key
     * @return value
     */
    public String get(String key) {
        try {
            Object obj = redisTemplate.opsForValue().get(key);
            if (obj != null) {
                return String.valueOf(obj);
            }
        } catch (Exception e) {
            log.warn("# redis get操作异常", e);
        }
        return null;
    }

    public boolean set(String key, Object obj) {
        try {
            String value = JacksonUtil.marshallToString(obj);
            return set(key, value);
        } catch (Exception e) {
            log.warn("# redis set操作异常", e);
            return false;
        }
    }

    public boolean set(String key, Object obj, long expire) {
        try {
            String value = JacksonUtil.marshallToString(obj);
            return set(key, value, expire);
        } catch (Exception e) {
            log.warn("# redis set操作异常", e);
            return false;
        }
    }

    public <T> T get(String key, Class<T> clazz) {
        try {
            String value = get(key);
            if (StringUtils.isBlank(value)) {
                return null;
            }
            return JacksonUtil.jsonToObject(value, clazz);
        } catch (Exception e) {
            log.warn("# redis get操作异常", e);
            return null;
        }
    }

    public <T> T get(String key, TypeReference<T> clazz) {
        try {
            String value = get(key);
            if (StringUtils.isBlank(value)) {
                return null;
            }
            return JacksonUtil.jsonToObject(value, clazz);
        } catch (Exception e) {
            log.warn("# redis get操作异常", e);
            return null;
        }
    }

    /**
     * DELETE操作
     *
     * @param key
     * @return 是否成功
     */
    public boolean delete(String key) {
        try {
            return redisTemplate.delete(key);
        } catch (Exception e) {
            log.warn("# redis delete操作异常", e);
            return false;
        }
    }

    public boolean exists(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            log.error("# redis exists操作异常", e);
            return false;
        }
    }

    /**
     * SETNX操作
     *
     * @param key
     * @param value
     * @param expireTime（单位：秒）
     * @return 是否成功
     */
    public boolean setNx(String key, String value, long expireTime) {
        try {
            return redisTemplate.opsForValue().setIfAbsent(key, value, expireTime, TimeUnit.SECONDS);
        } catch (Exception e) {
            log.error("# redis setnx操作异常", e);
            return false;
        }

    }

    /**
     * EXPIRE操作
     *
     * @param key
     * @param expireTime
     * @return
     */
    public boolean expire(String key, long expireTime) {
        try {
            return redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
        } catch (Exception e) {
            log.error("# redis expire操作异常", e);
            return false;
        }
    }

    /**
     * GETEXPIRE操作
     *
     * @param key
     * @return
     */
    public long getExpire(String key) {
        try {
            return redisTemplate.getExpire(key, TimeUnit.SECONDS);
        } catch (Exception e) {
            log.error("# redis getExpire操作异常", e);
            return 0;
        }
    }

    public long sAdd(String key, Object... member) {
        try {
            return redisTemplate.opsForSet().add(key, member);
        } catch (Exception e) {
            log.error("# redis sAdd操作异常", e);
            return 0L;
        }
    }

    public boolean sIsMember(String key, Object member) {
        try {
            return redisTemplate.opsForSet().isMember(key, member);
        } catch (Exception e) {
            log.error("# redis sAdd操作异常", e);
            return false;
        }
    }


    public double zIncrementScore(String key, Object member, double increment) {
        try {
            return redisTemplate.opsForZSet().incrementScore(key, member, increment);
        } catch (Exception e) {
            log.error("# redis zIncrementScore操作异常", e);
            return 0;
        }
    }

    public double zRemove(String key, Object... member) {
        try {
            return redisTemplate.opsForZSet().remove(key, member);
        } catch (Exception e) {
            log.error("# redis zRemove操作异常", e);
            return 0;
        }
    }

    public Long zCard(String key) {
        try {
            return redisTemplate.opsForZSet().zCard(key);
        } catch (Exception e) {
            log.error("# redis zCard操作异常", e);
            return 0L;
        }
    }

    public Set<Object> zReverseRange(String key, long begin, long end) {
        try {
            return redisTemplate.opsForZSet().reverseRange(key, begin, end);
        } catch (Exception e) {
            log.error("# redis zReverseRange操作异常", e);
            return null;
        }
    }

    public long increment(String key, long value) {
        try {
            return redisTemplate.opsForValue().increment(key, value);
        } catch (Exception e) {
            log.error("# redis increment操作异常", e);
            return 0L;
        }
    }

}
