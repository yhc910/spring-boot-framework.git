package com.yhc.server.consts;

public interface SystemValue {

    String APPLICATION_NAME = "spring-boot-framework";

    String ABBREVIATION_APPLICATION_NAME = "sbf";

    int LOGIN_MSG_CODE_ERROR_LIMIT = 5;

}
