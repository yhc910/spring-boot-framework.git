package com.yhc.server.consts;


public interface RedisPrefixKey {

    String REGULAR_PREFIX = SystemValue.ABBREVIATION_APPLICATION_NAME + ":";

    /**
     * 用户token
     * userId
     */
    String LOGIN_USER_TOKEN = REGULAR_PREFIX + "login:user:token:%s";


    /**
     * 短信验证码发送标识
     * mobile
     */
    String SMS_CODE_SEND_FLAG = REGULAR_PREFIX + "smsCode:send:flag:%s";

    /**
     * 用户登录短信验证码
     * mobile
     */
    String SMS_CODE_LOGIN= REGULAR_PREFIX + "smsCode:login:%s";

    /**
     * 用户登录短信验证码发送限制
     * mobile
     */
    String LOGIN_MSG_CODE_FLAG = REGULAR_PREFIX + "login:msgCode:flag:%s";

    /**
     * 用户登录图片验证码
     * random
     */
    String LOGIN_CAPTCHA = REGULAR_PREFIX + "login:captcha:%s";

    /**
     * 短信验证码错误次数（达到上限，账号锁定，失效时间：10分钟）
     * mobile
     */
    String SMS_CODE_LOGIN_ERROR_NUMBER = REGULAR_PREFIX + "smsCode:login:error:number:%s";

}
