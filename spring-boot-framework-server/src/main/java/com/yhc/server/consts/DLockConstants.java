package com.yhc.server.consts;

/**
 * 分布式锁
 */
public interface DLockConstants {
    /**
     * 任务完成分布式锁
     * param1: action
     * params2: userId
     */
    String DLOCK_CALLBACK_TASK = "dlock:callback:task:%s:%s";

}
