package com.yhc.server.consts;

/**
 * 请求头
 */
public interface RequestHeader {

    String REQUEST_ID = "requestId";
    String TX_TIME = "txTime";

    String USER_ID = "userId";

    String token = "Authorization";
}
