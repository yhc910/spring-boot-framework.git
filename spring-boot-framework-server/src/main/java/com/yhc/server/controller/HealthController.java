package com.yhc.server.controller;

import com.yhc.common.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 检测接口，检测服务是否健康
 */
@RestController
public class HealthController {

    @Value("${spring.application.name}")
    String applicationName;

    @RequestMapping("/health")
    public Map<String, Object> health() {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("time", DateTimeUtil.getDateTimeStrNow());
        result.put("application-name", applicationName);
        result.put("status", 200);
        return result;
    }


}
