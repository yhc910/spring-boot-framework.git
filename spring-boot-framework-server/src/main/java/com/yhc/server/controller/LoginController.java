package com.yhc.server.controller;

import com.yhc.api.LoginApi;
import com.yhc.api.request.LoginRequest;
import com.yhc.api.request.SendMsgRequest;
import com.yhc.api.response.LoginResponse;
import com.yhc.server.service.LoginService;
import com.yhc.common.dto.BaseResult;
import com.yhc.common.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("login")
public class LoginController implements LoginApi {

    @Autowired
    LoginService loginService;

    @Override
    public BaseResult<?> sendMsgCode(SendMsgRequest request) {
        loginService.sendMsg(request.getMobile());
        return ResultUtil.ok();
    }

    @Override
    public void getImageVerify(String randomStr) {
        loginService.getCaptcha(randomStr);
    }

    @Override
    public BaseResult<LoginResponse> login(LoginRequest request) {
        LoginResponse response = loginService.login(request);
        return ResultUtil.ok(response);
    }

}
