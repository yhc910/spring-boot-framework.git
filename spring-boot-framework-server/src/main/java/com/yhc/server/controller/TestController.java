package com.yhc.server.controller;

import com.yhc.server.feign.RemoteApi;
import com.yhc.api.TestApi;
import com.yhc.common.dto.BaseResult;
import com.yhc.common.util.ResultUtil;
import com.yhc.server.service.ITestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("test")
@RestController
public class TestController implements TestApi {

    @Autowired
    ITestService testService;

    @Autowired
    RemoteApi remoteApi;

    @Override
    public BaseResult<?> set(long id) {
        testService.set(id);
        return ResultUtil.ok();
    }

    @Override
    public BaseResult<String> get(long id) {
        return ResultUtil.ok(testService.get(id));
    }

    @Override
    public BaseResult<String> remote() {
        return ResultUtil.ok(remoteApi.remote());
    }
}
