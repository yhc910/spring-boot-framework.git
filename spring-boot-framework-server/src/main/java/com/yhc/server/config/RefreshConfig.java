package com.yhc.server.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@RefreshScope
public class RefreshConfig {

    @Value("#{'${interface.no.auth}'.split(',')}")
    List<String> noAuthInterface;

}
