//package com.yhc.server.config;
//
//import com.yhc.server.feign.RemoteApi;
//import com.yhc.server.interceptor.WebClientInterceptor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.reactive.function.client.WebClient;
//import org.springframework.web.reactive.function.client.support.WebClientAdapter;
//import org.springframework.web.service.invoker.HttpServiceProxyFactory;
//
//@Configuration
//public class WebClientConfig {
//
//
//    /*    @Autowired
//        ReactorLoadBalancerExchangeFilterFunction reactorLoadBalancerExchangeFilterFunction;*/
//    @Autowired
//    WebClientInterceptor webClientInterceptor;
//
//    @Bean
//    HttpServiceProxyFactory httpServiceProxyFactory() {
//        WebClient client = WebClient.builder().baseUrl("http://spring-boot-framework").filter(webClientInterceptor).build();
//        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builder(WebClientAdapter.forClient(client)).build();
//        return factory;
//    }
//
//    @Bean
//    RemoteApi remoteApiWebClient(HttpServiceProxyFactory httpServiceProxyFactory) {
//        return httpServiceProxyFactory.createClient(RemoteApi.class);
//    }
//
//
//}
