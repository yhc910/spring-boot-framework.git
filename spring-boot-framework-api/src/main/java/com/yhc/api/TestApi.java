package com.yhc.api;

import com.yhc.common.dto.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "sbf", contextId = "test", path = "test")
public interface TestApi {

    /**
     * 登录功能不是本次重点， 故返回特定token
     *
     * @return
     */
    @GetMapping("/set")
    BaseResult<?> set(@RequestParam long id);

    @RequestMapping("/get")
    BaseResult<String> get(@RequestParam long id);

    @RequestMapping("/remote")
    BaseResult<String> remote();
}
