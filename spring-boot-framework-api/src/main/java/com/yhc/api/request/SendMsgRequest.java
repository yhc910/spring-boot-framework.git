package com.yhc.api.request;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Pattern;

@Data
@ToString
public class SendMsgRequest {

    @Pattern(regexp = "^1[3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式错误")
    private String mobile;

}
