package com.yhc.api.request;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@ToString
public class LoginRequest {

    @NotBlank(message = "randomStr cannot be empty")
    private String randomStr;

    @Pattern(regexp = "^1[3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式错误")
    private String mobile;

    @NotBlank(message = "smsCode cannot be empty")
    private String smsCode;

    @NotBlank(message = "captcha cannot be empty")
    private String captcha;
}
