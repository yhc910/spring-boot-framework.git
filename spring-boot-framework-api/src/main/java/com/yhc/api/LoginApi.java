package com.yhc.api;

import com.yhc.api.request.LoginRequest;
import com.yhc.api.request.SendMsgRequest;
import com.yhc.api.response.LoginResponse;
import com.yhc.common.dto.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@FeignClient(name = "sbf", contextId = "login", path = "login")
public interface LoginApi {

    @PostMapping
    BaseResult<LoginResponse> login(@RequestBody @Valid LoginRequest request);

    @PostMapping("sendMsgCode")
    BaseResult<?> sendMsgCode(@RequestBody @Valid SendMsgRequest request);

    @GetMapping("getCaptcha")
    void getImageVerify(@RequestParam String randomStr);

}
