package com.yhc.server.mapper.typeHandler;

import com.yhc.common.util.HuAesUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Slf4j
public class SecretNumberTypeHandler implements TypeHandler<String>{
    @Override
    public void setParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {
        log.info("# SecretNumberTypeHandler setParameter: {}", parameter);
        String value = HuAesUtil.encryptSecret(parameter);
        log.info("# SecretNumberTypeHandler setParameter2: {}", value);
        ps.setString(i, value);
    }

    @Override
    public String getResult(ResultSet rs, String columnName) throws SQLException {
        log.info("# SecretNumberTypeHandler getResult: {}", columnName);
        String value = rs.getString(columnName);
        return HuAesUtil.decryptSecret(value);
    }

    @Override
    public String getResult(ResultSet rs, int columnIndex) throws SQLException {
        log.info("# SecretNumberTypeHandler getResult: {}", columnIndex);
        String value = rs.getString(columnIndex);
        return HuAesUtil.decryptSecret(value);
    }

    @Override
    public String getResult(CallableStatement cs, int columnIndex) throws SQLException {
        log.info("# SecretNumberTypeHandler getResult: {}", columnIndex);
        String value = cs.getString(columnIndex);
        return HuAesUtil.decryptSecret(value);
    }
}
