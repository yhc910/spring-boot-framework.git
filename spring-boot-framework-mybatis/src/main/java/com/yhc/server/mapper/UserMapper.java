package com.yhc.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yhc.server.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
}
