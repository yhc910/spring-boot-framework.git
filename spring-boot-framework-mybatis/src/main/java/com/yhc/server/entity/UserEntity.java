package com.yhc.server.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yhc.server.mapper.typeHandler.SecretNumberTypeHandler;
import com.yhc.server.mapper.typeHandler.SecretPwdTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("user")
public class UserEntity extends BaseEntity {

    @TableField(typeHandler = SecretNumberTypeHandler.class)
    private String mobile;
    @TableField(typeHandler = SecretPwdTypeHandler.class)
    private String password;
    private String name;

}
