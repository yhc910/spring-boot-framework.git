package com.yhc.server.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yhc.server.entity.UserEntity;
import com.yhc.server.manager.UserManager;
import com.yhc.server.mapper.UserMapper;
import com.yhc.common.util.HuAesUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserManagerImpl extends ServiceImpl<UserMapper, UserEntity> implements UserManager {
    @Override
    public UserEntity getOneByMobile(String mobile) {
        LambdaQueryWrapper<UserEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserEntity::getMobile, HuAesUtil.encryptSecret(mobile));
        return getOne(queryWrapper);
    }
}
