package com.yhc.server.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yhc.server.entity.UserEntity;

public interface UserManager extends IService<UserEntity> {

    UserEntity getOneByMobile(String mobile);

}
