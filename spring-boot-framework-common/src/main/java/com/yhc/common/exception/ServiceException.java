package com.yhc.common.exception;


import lombok.Getter;

@Getter
public class ServiceException extends RuntimeException {

    private int errorCode;
    private String errorMessage;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(int errorCode, String errorMessage) {
        super(errorCode + "-" + errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ServiceException(ResponseEnum response) {
        super(response.getCode() + "-" + response.getMessage());
        this.errorCode = response.getCode();
        this.errorMessage = response.getMessage();
    }


}
