package com.yhc.common.exception;

import lombok.Getter;

/**
 * 这里定义所有微服务错误码的范围，避免串码
 */
public enum ResponseEnum {

    OK(100, "请求成功"),
    ERROR(500, "系统异常"),
    EXCEPTION_CUSTOMIZE(4100, "自定义错误信息"),
    /**
     * 公共错误码（4101-4999）
     */
    INVALID_REQUEST(4101, "非法请求"),

    /**
     * 子服务错误码
     */

    /**
     * service-a错误码（400101-400199）
     */
    EXCEPTION_SERVICE_A(4001, "service-a service error"),

    /**
     * model-b错误码（400201-400299）
     */
    EXCEPTION_SERVICE_B(4002, "service-b service error"),

    ;

    @Getter
    int code;
    @Getter
    String message;

    ResponseEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

}
