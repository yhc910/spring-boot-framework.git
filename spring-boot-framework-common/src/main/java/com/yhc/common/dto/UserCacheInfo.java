package com.yhc.common.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * 用户缓存信息
 */
@Data
@ToString
@Builder
public class UserCacheInfo {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户名
     */
    private String name;

    /**
     * 手机号
     */
    private String mobile;

}
