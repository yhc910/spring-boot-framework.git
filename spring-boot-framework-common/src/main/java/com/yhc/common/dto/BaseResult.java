package com.yhc.common.dto;

import com.yhc.common.exception.ResponseEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 所有微服务统一返回结构
 *
 * @param <T>
 */
@Data
@ToString
@AllArgsConstructor
public class BaseResult<T> {

    int code;
    String message;
    T data;

    public Boolean isSuccess() {
        return ResponseEnum.OK.getCode() == code;
    }

}
