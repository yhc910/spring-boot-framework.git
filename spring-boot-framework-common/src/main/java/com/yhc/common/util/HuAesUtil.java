package com.yhc.common.util;

import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class HuAesUtil {

    private static final String aes_key = "YHC_SBF_DEMO_AES";
    private static final AES aes;

    static {
        aes = new AES(Mode.ECB, Padding.PKCS5Padding, aes_key.getBytes());
    }

    public static String encryptStr(String content) {
        if (StringUtils.isBlank(content)) return null;
        try {
            return aes.encryptBase64(content);
        } catch (Exception e) {
            log.warn("# HuAesUtil-encryptStr fail: {}", e.getMessage());
        }
        return content;
    }

    public static String encryptStrHex(String content) {
        if (StringUtils.isBlank(content)) return null;
        try {
            return aes.encryptHex(content);
        } catch (Exception e) {
            log.warn("# HuAesUtil-encryptStr fail: {}", e.getMessage());
        }
        return null;
    }

    public static String decryptStr(String content) {
        if (StringUtils.isBlank(content)) return null;
        try {
            return aes.decryptStr(content);
        } catch (Exception e) {
            log.warn("# HuAesUtil-decryptStr fail: {}", e.getMessage());
        }
        return content;
    }

    public static String encryptSecret(String content) {
        if (StringUtils.isBlank(content) || content.length() < 11) {
            return content;
        }
        StringBuilder sb = new StringBuilder(content);
        sb.insert(3, 1);
        sb.insert(9, 1);
        log.info(sb.toString());
        String encryptStr = encryptStr(sb.toString());
        return encryptStr;
    }

    public static String decryptSecret(String content) {
        String decryptStr = decryptStr(content);
        if (StringUtils.isBlank(decryptStr) || decryptStr.length() < 11) {
            return decryptStr;
        }
        StringBuilder sb = new StringBuilder(decryptStr);
        sb.delete(3, 4);
        sb.delete(8, 9);
        return sb.toString();
    }

    public static String encryptStrWithSalt(String content) {
        if (StringUtils.isBlank(content)) return null;
        try {
            String encryptStr = encryptStr(content);
            log.info("# str:{}", encryptStr);
            StringBuilder contentNew = new StringBuilder();
            for (int i = 0; i < encryptStr.length(); i++) {
                char c = encryptStr.charAt(i);
                contentNew.append(c);
                if (i == 3) {
                    contentNew.append("1");
                } else if (i == 7) {
                    contentNew.append("m");
                } else if (i == 11) {
                    contentNew.append("2");
                } else if (i == 12) {
                    contentNew.append("c");
                } else if (i == 15) {
                    contentNew.append("8");
                }
            }
            return contentNew.toString();
        } catch (Exception e) {
            log.warn("# HuAesUtil-encryptStrWithSalt fail: {}", e.getMessage());
        }
        return content;
    }

    public static String encryptStrWithSaltHex(String content) {
        if (StringUtils.isBlank(content)) return null;
        try {
            String encryptStr = encryptStrHex(content);
            log.info("# str:{}", encryptStr);
            StringBuilder contentNew = new StringBuilder();
            for (int i = 0; i < encryptStr.length(); i++) {
                char c = encryptStr.charAt(i);
                contentNew.append(c);
                if (i == 3) {
                    contentNew.append("1");
                } else if (i == 7) {
                    contentNew.append("m");
                } else if (i == 11) {
                    contentNew.append("2");
                } else if (i == 12) {
                    contentNew.append("c");
                } else if (i == 15) {
                    contentNew.append("8");
                }
            }
            return contentNew.toString();
        } catch (Exception e) {
            log.warn("# HuAesUtil-encryptStrWithSalt fail: {}", e.getMessage());
        }
        return content;
    }

    public static String decryptStrWithSalt(String content) {
        if (StringUtils.isBlank(content)) return null;
        try {
            StringBuilder contentNew = new StringBuilder(content);
            contentNew.replace(4, 5, "");
            contentNew.replace(8, 9, "");
            contentNew.replace(12, 13, "");
            contentNew.replace(13, 14, "");
            contentNew.replace(16, 17, "");
            log.info("# str:{}", contentNew);
            return decryptStr(contentNew.toString());
        } catch (Exception e) {
            log.warn("# HuAesUtil-encryptStrWithSalt fail: {}", e.getMessage());
        }
        return content;
    }

    public static void main(String[] args) {
        System.out.println(HuAesUtil.encryptSecret("19876543210"));
    }

}
