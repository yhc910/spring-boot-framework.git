package com.yhc.common.util;

import com.yhc.common.dto.BaseResult;
import com.yhc.common.exception.ResponseEnum;
import com.yhc.common.exception.ServiceException;

/**
 * 统一返回工具类
 *
 * @author yhc
 */
public class ResultUtil{

    // 错误码添加固定前缀，标识返回错误的微服务模块
//    private static String ERROR_MESSAGE_PREFIX = "SBF:";

    public static <T> BaseResult<?> ok() {
        return new BaseResult(ResponseEnum.OK.getCode(), ResponseEnum.OK.getMessage(), null);
    }

    public static <T> BaseResult<T> ok(T data) {
        return new BaseResult(ResponseEnum.OK.getCode(), ResponseEnum.OK.getMessage(), data);
    }

    public static <T> BaseResult<?> fail(String errorMessage) {
        return new BaseResult(ResponseEnum.EXCEPTION_CUSTOMIZE.getCode(), errorMessage, null);
    }

    public static <T> BaseResult<?> fail(ResponseEnum resp) {
        return new BaseResult(resp.getCode(), resp.getMessage(), null);
    }

    public static <T, E extends ServiceException> BaseResult<?> fail(E se) {
        return new BaseResult(se.getErrorCode(), se.getErrorMessage(), null);
    }

    public static <T> BaseResult<?> fail(ResponseEnum resp, T data) {
        return new BaseResult(resp.getCode(), resp.getMessage(), data);
    }

    public static <T> BaseResult<?> error() {
        return new BaseResult(ResponseEnum.ERROR.getCode(), ResponseEnum.ERROR.getMessage(), null);
    }

}
