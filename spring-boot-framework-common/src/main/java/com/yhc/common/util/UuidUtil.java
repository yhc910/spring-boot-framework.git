package com.yhc.common.util;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.lang.generator.UUIDGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;

@Slf4j
public class UuidUtil {

    /**
     * 获取不带"-"的uuid
     *
     * @return
     */
    public static String getUuid() {
        return UUID.fastUUID().toString(true);
    }

    /**
     * 生产requestId
     *
     * @return
     */
    public static String generateRequestId() {
        return RandomStringUtils.randomAlphanumeric(10);
    }

}
