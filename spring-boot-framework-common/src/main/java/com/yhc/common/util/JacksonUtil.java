package com.yhc.common.util;

import cn.hutool.core.lang.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonUtil {

    private static final Logger log = LoggerFactory.getLogger(JacksonUtil.class);

    public static ObjectMapper mapper = new ObjectMapper();

    public static <T> String marshallToString(Object obj) {
        Assert.notNull(obj);
        try {
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            log.error("# MarshallToString error", obj);
        }
        return null;
    }

    public static <T> T jsonToObject(String json, Class<T> clazz) {
        Assert.notEmpty(json);
        try {
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            log.error("# JsonToObject error", e);
        }
        return null;
    }

    public static <T> T jsonToObject(String json, TypeReference<T> clazz) {
        Assert.notEmpty(json);
        try {
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            log.error("# JsonToObject error", e);
        }
        return null;
    }

    public static void main(String[] args) {
        Long s = 10l;
        String str = marshallToString(s);
        System.out.println(str);

        String str2 = "10";
        System.out.println(str2);
    }

}
